    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Add Installment</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <form action="" method="post" class="col-12 p-0">
                <div class="flex-column col-12 main-padding-l pr-0">
                    <div class="col-12 p-0">
                        <p class="c-text-2 soft-title medium-weight">Buyer Name</p>
                        <!-- <input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="name" placeholder="Name..."> -->
                        <select name="name" id="name" style="width: 100%" class="dropdown-select2 c-text-2 search-fill" >
                            <option value="">Select Buyer</option>
                        </select>
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Date</p>
                        <input type="date" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="date" placeholder="Date...">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Debt</p>
                        <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="debt" placeholder="Debt...">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Payment</p>
                        <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="payment" placeholder="Payment...">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Status</p>
                        <input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="status" placeholder="Status...">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Additional Info</p>
                        <textarea class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" id="info" style="width: 100%; min-height: 150px;"></textarea>
                    </div>
                    <button class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2" type="submit">Add Material</button>
                </div>
            </form>
        </div>
</div>

<div class="modal fade" id="addMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Add Incoming Material</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <p class="c-text-2 soft-title regular-weight">Material Name</p>
            <input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name...">
        </div>
        <div class="col-12 p-0 mt-3">
            <p class="c-text-2 soft-title regular-weight">Material Quantity</p>
            <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative c-text-2 mr-3 medium-weight" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn-modal-positive c-text-2 medium-weight">Add Installment</button>
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function () {
        $('.dropdown-select2').select2();
        set_filter();

        function set_filter() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/customer",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].customer_name+'>'+obj.data[i].customer_name+'</option>';
                        $("#name").html(payload);
                    }
                }
            });
        }
    });
</script>
