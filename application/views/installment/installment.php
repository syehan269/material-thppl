    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <input class="search-fill col-6 border-0" type="text" name="search" id="search" placeholder="Search">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
        
        <div class="ml-auto d-inline-flex">
            <div class="d-inline-flex my-auto">
                <p class="my-auto c-text-2 mr-3 medium-weight">Date</p>
                <input id ="date" type="date" class="my-auto c-text-2 search-fill" placeholder="Date" style="padding: 10px 12px">
            </div>
            <!-- <div class="d-inline-flex my-auto ml-3">
                <p class="my-auto mr-3 c-text-2 medium-weight">Status</p>
                <div class="dropdown c-text-2">
                    <select id ="status" class="c-dropdown" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="">All</option>
                        <option class="dropdown-item" value="Finished">Finished</option>
                        <option class="dropdown-item" value="Incomplete">Incomplete</option>
                    </select>
                </div>
            </div> -->
            <div class="d-flex my-auto ml-3">
                <button class="btn-filter c-color-primary" id="filter">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                </button>
            </div>
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="main-table">
            <thead>
                <tr class="t-header col-12">
                    <td class="p-3 primary-title boldest-weight">No</td>
                    <td class="p-3 primary-title boldest-weight">Transaction ID</td>
                    <td class="p-3 primary-title boldest-weight">Buyer Name</td>
                    <td class="p-3 primary-title boldest-weight" >Date Transaction</td>
                    <td class="p-3 primary-title boldest-weight d-none" >Debt Amount</td>
                    <td class="p-3 primary-title boldest-weight" >Payment Amount</td>
                    <td class="p-3 primary-title boldest-weight" >Service Type</td>
                    <td class="p-3 primary-title boldest-weight d-none" >Status</td>
                    <td class="p-3 primary-title boldest-weight" >Action</td>
                </tr>
            </thead>
            <tbody id="tb_installment">
                
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="upCicilan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Edit Installment Payment</p>
            </div>
            <div class="modal-body c-main-background">
                <div class="col-12 p-0" id="dateCount">
                    <p class="c-text-2 soft-title regular-weight">Payment Date</p>
                    <div class="d-inline-flex col-12 p-0">
                        <input type="date" id="cicilanee" class="col-11 c-text-2 search-fill main-padding-l main-padding-r" placeholder="...">
                        <a onclick="" class="my-auto">
                            <button class="ml-2 basic-btn c-soft-background" >
                                <i class="bx bx-check-double bx-xs primary-title" style="margin-top: 5px"></i>
                            </button>
                        </a >
                    </div>
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn-modal-positive medium-weight">Confirm</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editCicilan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Update Installment</p>
            </div>
            <div class="modal-body c-main-background">
                <div class="col-12 p-0" id="countPay">
                    <!-- <p class="c-text-2 soft-title regular-weight">Payment Date</p>
                    <input type="date" id="cicilan" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity..."> -->
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn-modal-positive medium-weight">Confirm</button>
            </div>
            </div>
        </div>
    </div>

</div>
<div class="modal fade" id="prevIns" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Preview Installment</p>
            </div>
            <div class="modal-body c-main-background px-4">
                <div class="col-12 p-0 ">
                    <p class="c-text-3 soft-title regular-weight">Buyer Name</p>
                    <input disabled id="prvName" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Transaction Date</p>
                    <input disabled id="prvDate" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Material Sold</p>
                    <div class=" col-12 custom-card p-3" >
                        <table class="col-12 p-3" width="100%">
                            <thead class="t-header primary-title">
                                <tr>
                                    <th class="p-3 c-text-3 boldest-weight text-center">No</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Material Name</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Brand</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Quantity</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Price(1)</th>
                                </tr>
                            </thead>
                            <tbody id="tbPrv">
                                
                            </tbody>
                            <tfoot class="t-header primary-title" id="footMat">
                                
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Installment Payment</p>
                    <div class=" col-12 custom-card p-3">
                        <table class="col-12 p-3" width="100%">
                            <thead class="t-header primary-title">
                                <tr>
                                    <th class="p-3 c-text-3 boldest-weight text-center">No</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Due Date</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Payment Amount</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Payment Status</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="tbPrvCredit">
                                
                            </tbody>
                            <tfoot class="t-header primary-title" id="footPay">

                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Installent Information</p>
                    <textarea disabled id="prvInfo" class="search-fill c-card c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" style="min-height: 150px; width: 100%"></textarea>
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var table = "";

        $("#search").on("input", function () {
            search(2, this.value);
        });

        $("#date").on("input change", function () {
            var getDate = this.value;
            var split1 = getDate.split("-");
            var finDate = split1[1]+"/"+split1[2]+"/"+split1[0];
            search(3, finDate);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#main-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#date").val("");
            $("#search").val("");
        });

        setTable();

        function dataTable(){
            table = $('#main-table').DataTable({
                "lengthChange": false,
                //"pageLength": 50,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function setTable() {
            $.ajax({
                type  : 'GET',
                url   : 'http://153.92.4.88:8080/transaction-out',
                async : true,
                dataType : 'text',
                success : function(data){
                    var html = '';
                    var i;
                    var text = data;
                    obj = JSON.parse(text);
                    for(i=0; i<obj.data.length; i++){
                        
                        var getDate = obj.data[i].trans_out_due_date;
                        var split = getDate.split("T");
                        var split1 = split[0].split("-");

                        var setDate = split1[1]+"/"+split1[2]+"/"+split1[0];
                        var result = obj.data[i].trans_out_payment_amount;
                        var amount = "Rp "+result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        var type = obj.data[i].trans_out_payment_type;
                        var deli = obj.data[i].trans_out_delivery_type;
                        var cashPay = "";
                        var transId = obj.data[i].trans_out_id;
                        var splitId = transId.split("-");
                        var credit_total = 0;
                        var credit_count = 0;
                        var credit_item = 0;
                        var credit_tod = 0;

                        //addCicilanField(transId);

                        if (type == "Credit") {

                            html += '<tr>'+
                                        '<td class="p-3 c-text-2">'+(i+1)+'</td>'+
                                        '<td class="p-3 c-text-2">'+obj.data[i].trans_out_id+'</td>'+
                                        '<td class="p-3 c-text-2">'+obj.data[i].trans_out_customer_name+'</td>'+
                                        '<td class="p-3 c-text-2">'+setDate+'</td>'+
                                        '<td class="p-3 c-text-2 text-credit d-none" id="debt-'+splitId[1]+'"></td>'+
                                        '<td class="p-3 c-text-2">'+amount+'</td>'+
                                        '<td class="p-3 c-text-2">'+deli+'</td>'+
                                        '<td class="p-3 c-text-2  d-none" id="status-'+splitId[1]+'"></td>'+
                                        '<td>'+
                                            '<a href="#" onclick="setPreview(\''+transId+'\')">'+
                                                '<button class="ml-2 basic-btn-1 c-color-primary " >'+
                                                    '<i class="bx bx-show text-white" style="margin-top: 5px"></i>'+
                                                '</button>'+
                                            '</a >'+
                                            '<a href="#" onclick="addCicilanField(\''+transId+'\')"">'+
                                                '<button class="ml-2 basic-btn c-soft-background" >'+
                                                    '<i class="bx bxs-pencil primary-title " style="margin-top: 5px"></i>'+
                                                '</button>'+
                                            '</a >'+
                                            //addCicilanField(\''+transId+'\')
                                            // '<a href="#" onclick="showInstall()">'+
                                            //     '<button class="ml-2 basic-btn c-soft-background" >'+
                                            //         '<i class="bx bxs-pencil primary-title " style="margin-top: 5px"></i>'+
                                            //     '</button>'+
                                            // '</a >'+
                                        '</td>'+
                                    '</tr>';
                            setQuant(transId);
                        }

                    }
                    $('#tb_installment').html(html);
                    dataTable();
                }
            });
        }

        function getCreditStatus(id) {
            
            $.ajax({
                type: "get",
                url: "http://153.92.4.88:8080/transaction-out/"+id,
                dataType: "text",
                async : true,
                success: function (response) {
                    var obj = JSON.parse(response);
                    var count1 = 0;

                    if (obj.data.credit.length > 0) {
                        count1 = obj.data.credit.length;
                        console.log(id+" "+count1);
                        console.log("true");
                        
                    }
                    
                }
            });
        }    

    });

    function setQuant(transId) {
        $.ajax({
            type: "get",
            url: "http://153.92.4.88:8080/transaction-out/"+transId,
            async: true,
            dataType: "text",
            success: function (response) {
                const obj = JSON.parse(response);
                var getTotal = 0;
                var getDebt = 0;
                var id = transId.split("-");
                var getCreditLength = obj.data.credit.length;
                var statusId = "#status-"+id[1];
                var debtId = "#debt-"+id[1];
                
                for (let i = 0; i < obj.data.credit.length; i++) {
                    const context = obj.data.credit[i];
                    var getStatus = context.credit_paid;
                    var getRawDebt = context.credit_amount;

                    if (getStatus == 1) {
                        getTotal += 1;
                    }else{
                        getDebt += getRawDebt;
                    }

                }

                if (getDebt <= 0 || getDebt == null) {
                    getDebt = 0;
                }

                var finalStatus = getTotal+"/"+getCreditLength;
                var finalDebt = "Rp "+getDebt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                if (getTotal == getCreditLength) {
                    var payloadStatus = '<span class="cash-status">Paid</span>';
                }else{
                    var payloadStatus = '<span class="credit-status">'+finalStatus+'</span>';
                }

                if (getDebt == 0) {
                    var payloadDebt = '<span class="cash-status">'+finalDebt+'</span>';    
                }else{
                    var payloadDebt = '<span class="credit-status">'+finalDebt+'</span>';
                }

                $(statusId).html(payloadStatus);
                $(debtId).html(payloadDebt);

            }
        });
    }

    function addCicilanField(id) {
        $.ajax({
            type: "get",
            async : true,
            url: "http://153.92.4.88:8080/transaction-out/"+id,
            dataType: "text",
            success: function (response) {
                var obj = JSON.parse(response);
                var getCountCredit = obj.data.credit.length;
                var payload = '<p class="c-text-2 soft-title mt-3 text-center regular-weight">No Payment Added</p>';
                console.log(getCountCredit);
                
                if (getCountCredit > 0) {
                    payload = "";
                    for (let i = 0; i < getCountCredit; i++) {
                        const element = getCountCredit;
                        var getCreditDate = obj.data.credit[i].credit_due_date;
                        var getAmount = obj.data.credit[i].credit_amount;
                        var finAmount = "Rp "+getAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        var split = getCreditDate.split("T");
                        var split1 = split[0].split("-");
                        var setDate = split1[1]+"/"+split1[2]+"/"+split1[0];

                        var temp = '<p id="cicilan-number-'+(i+1)+'" class="field-cicilan c-text-2 soft-title mt-2 regular-weight">Payment '+(i + 1)+'</p>' +
                                '<input type="date" id="cicilan'+(i + 1)+'" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" value="'+split[0]+'">';

                        var setButtonStatus = '';

                        if (obj.data.credit[i].credit_paid == 1) {
                            setButtonStatus = '<button class="ml-2 basic-btn c-soft-background" onclick="compCredit(\''+id+'\')" >'+
                                '<i class="bx bx-check-double bx-xs primary-title" style="margin-top: 5px"></i>'+
                            '</button>';
                        }else{
                            setButtonStatus = '<button class="ml-2 basic-btn c-color-primary" onclick="compCredit(\''+id+'\')" >'+
                                '<i class="bx bx-check-double bx-xs text-white" style="margin-top: 5px"></i>'+
                            '</button>';
                        }

                        payload += 
                                '<p id="cicilan-number-'+(i+1)+'" class="field-cicilan c-text-2 soft-title mt-2 regular-weight">Payment '+(i + 1)+'</p>' +
                                '<div class="d-inline-flex col-12 p-0">'+
                                    '<input type="date" id="cicilan'+(i + 1)+'" class="col-11 c-text-2 search-fill main-padding-l main-padding-r" placeholder="..." value="'+split[0]+'">'+
                                    '<a onclick="" class="my-auto">'+
                                        setButtonStatus+
                                    '</a >'+
                                '</div>'+
                                '<p id="cicilan-amount-'+(i+1)+'" class="field-cicilan c-text-2 soft-title mt-2 regular-weight">Payment Amount '+(i + 1)+'</p>' +
                                '<input type="number" id="cicilan'+(i + 1)+'" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" value="'+getAmount+'">';

                                
                       
                    }
                }
                $("#countPay").html(payload);
                showEdit();
            }
        });
    }

    function setPreview(id) {
        $.ajax({
            type: "get",
            url: "http://153.92.4.88:8080/transaction-out/"+id,
            async: true,
            dataType: "text",
            success: function (response) {  
                var obj = JSON.parse(response);
                var getId = obj.data.transaction.trans_out_id;
                var getAmount = obj.data.transaction.trans_out_payment_amount;
                var getType = obj.data.transaction.trans_out_payment_type;
                var getInfo = obj.data.transaction.trans_out_additional_info;
                var getName = obj.data.transaction.trans_out_customer_name;

                var rawDate = obj.data.transaction.trans_out_due_date;
                var split = rawDate.split("T")
                var split1 = split[0].split("-")
                var getDate = split1[1]+"/"+split1[2]+"/"+split1[0];
                var payload = "";
                var payload1 = "";
                var payloadMatFoot = "";
                var payloadCreFoot = "";
                var finalCount = "";
                var getCreditCount = 0;
                var getTotalCredit = obj.data.credit.length;
                var getTotalItem = 0;
                var amo = 0;
                var getTotalCre = 0;
                var getTotalDebt = 0;
                var getItemDebt = 0;
                
                

                for (let o = 0; o < obj.data.credit.length; o++) {
                    const context = obj.data.credit[o];
                    if (context.credit_paid == 1) {
                        getCreditCount += 1;
                    }
                }

                for (let a = 0; a < obj.data.detail.length; a++) {
                    const context = obj.data.detail[a];
                    amo +=context.detail_out_material_price;
                    getTotalItem += context.detail_out_material_amount;
                }

                var amount = "Rp "+getAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                finalCount = getCreditCount+" completed "+getTotalCredit+" left.";

                //$("#prvService").val(finalCount);
                //$("#prvAmount").val(amount);
                $("#prvInfo").val(getInfo);
                $("#prvDate").val(getDate);
                $("#prvTrans").val(getType);
                $("#prvName").val(getName);

                for (let i = 0; i < obj.data.detail.length; i++) {
                    const context = obj.data.detail[i];
                    var getMatName =  context.detail_out_merek_name;
                    var getMatQuant = context.detail_out_material_amount;
                    var getMat = context.detail_out_material_name;
                    var getPrice = context.detail_out_material_price;
                    var finalPrice = "Rp "+getPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    if (getMatName != null && getMatQuant != null) {

                        payload += '<tr>'+
                                    '<td class="p-2 c-text-2 text-center">'+(i+1)+'.</td>'+
                                    '<td class="p-2 c-text-2 text-center">'+getMat+'</td>'+
                                    '<td class="p-2 c-text-2 text-center">'+getMatName+'</td>'+
                                    '<td class="p-2 c-text-2 text-center">'+getMatQuant+' item </td>'+
                                    '<td class="p-2 c-text-2 text-center">'+finalPrice+'</td>'+
                            '</tr>';
                    }
                }

                payloadMatFoot = '<tr>'+
                                    '<td colspan="3" class="p-2 c-text-3 boldest-weight text-center">Total</td>'+
                                    '<td class="p-2 c-text-3 boldest-weight text-center"> '+getTotalItem+' item</td>'+
                                    '<td class="p-2 c-text-3 boldest-weight text-center"> '+amount+'</td>'+
                                '</tr>';

                for (let o = 0; o < obj.data.credit.length; o++) {
                        const context = obj.data.credit[o];
                        var getInfo = context.credit_additional_info;
                        var getDue = context.credit_due_date;
                        var splitc = getDue.split("T");
                        var split1 = splitc[0].split("-");
                        var getDateDue = split1[1]+"/"+split1[2]+"/"+split1[0];
                        var getStatusIns = context.credit_paid;
                        var getAmountIns = context.credit_amount;
                        var finalAm = "Rp "+getAmountIns.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        var creditAct = '';
                        getTotalCre += getAmountIns;

                        if (getStatusIns == 1) {
                            getStatusIns = "Paid";
                            creditAct = '<td class="p-2 c-text-2 text-center ">'+
                                                '<a onclick="">'+
                                                    '<button class="ml-2 basic-btn c-soft-background" >'+
                                                        '<i class="bx bx-check-double bx-xs primary-title" style="margin-top: 5px"></i>'+
                                                    '</button>'+
                                                '</a >'+
                                            '</td>';

                        }else{
                            getTotalDebt += getAmountIns;
                            getStatusIns = "Unpaid";
                            creditAct = '<td class="p-2 c-text-2 text-center ">'+
                                            '<a onclick="compCredit(\''+getId+'\')">'+
                                                '<button class="ml-2 basic-btn c-color-primary" >'+
                                                    '<i class="bx bx-check-double bx-xs text-white" style="margin-top: 5px"></i>'+
                                                '</button>'+
                                            '</a >'+
                                        '</td>';
                        }

                        payload1 += '<tr>'+
                                    '<td class="p-2 c-text-2 text-center">'+(o+1)+'.</td>'+
                                    '<td class="p-2 c-text-2 text-center">'+getDateDue+'</td>'+
                                    '<td class="p-2 c-text-2 text-center">'+finalAm+'</td>'+
                                    '<td class="p-2 c-text-2 text-center"><span class="credit-status">'+getStatusIns+'</span></td>'+
                                    creditAct+
                            '</tr>';
                    }

                    payloadPayFoot = '<tr>'+
                                    '<td colspan="3" class="p-2 c-text-3 boldest-weight text-center">Payment Total</td>'+
                                    '<td colspan="2" class="p-2 c-text-3 boldest-weight text-center"> Rp '+getTotalCre.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td colspan="3" class="p-2 c-text-3 boldest-weight text-center text-credit">Debt Total</td>'+
                                    '<td colspan="2" class="p-2 c-text-3 boldest-weight text-center text-credit"> Rp '+getTotalDebt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td>'+
                                '</tr>';

                    if (payload == null) {
                        payload1 = '<p class="c-text-2 soft-title mt-3 text-center regular-weight">No Payment Added</p>';
                    }

                    $("#footPay").html(payloadPayFoot);
                    $("#tbPrv").html(payload);
                    $("#footMat").html(payloadMatFoot);
                    $("#tbPrvCredit").html(payload1);

                showPrev()

            }
        });
        
    }

    function compCredit(id) {
        confirm("Update status payment "+id+" ?");
    }

    function showInstall() {
        $("#upCicilan").modal("show");
    }
    function showEdit() {
        $("#editCicilan").modal("show");
    }
    function showPrev() {
        $("#prevIns").modal("show");
    }
</script>