<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Material.</title>
</head>
<body class="">
    <div class="custom-card col-4 p-4 shadow d-flex flex-column justify-content-center mx-auto" style="background-color: #f4f4ff !important; margin-top: 120px;">
        <form action="<?php echo base_url() ?>index.php/Welcome/login" method="post">
            <p class="text-center medium-weight c-text-6">Login.</p>
            <input name="username" class="search-fill mt-2 px-3" type="text" id="username" placeholder="Username" style="width: 100%;">
            <input name="password" class="search-fill mt-3 px-3" type="password" id="password" placeholder="Password" style="width: 100%;">
            <button type="submit" class="c-text-3 mt-4 mx-auto btn-add c-color-primary text-white medium-weight" style="width: 100%;">
                Login
            </button>
        </form>
    </div>
</body>
</html>