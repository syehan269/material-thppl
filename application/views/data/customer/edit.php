    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Edit Customer</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
          <div class="flex-column col-12 main-padding-l pr-0">
              <div class="col-12 p-0">
                  <p class="c-text-2 soft-title medium-weight">Customer ID</p>
                  <input id="id" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="ID...">
              </div>
              <div class="col-12 p-0 mt-4">
                  <p class="c-text-2 soft-title medium-weight">Customer Name</p>
                  <input id="name" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name...">
              </div>
              <div class="col-12 p-0 mt-4">
                  <p class="c-text-2 soft-title medium-weight">Type</p>
                  <select name="name" id="type" style="width: 100%" class="p-2 dropdown-select2 c-text-2 search-fill" >
                    <option value="">Select Customer</option>
                    <option value="Distributor">Distributor</option>
                    <option value="Retail">Retail</option>
                    <option value="Special Retail">Special Retail</option>
                  </select>
              </div>
              <div class="col-12 p-0 mt-4">
                  <p class="c-text-2 soft-title medium-weight">Discount</p>
                  <select name="discount" id="discount" style="width: 100%" class="p-2 dropdown-select2 c-text-2 search-fill" >
                    <option value="">Select Discount</option>                    
                  </select>
              </div>
              <div class="col-12 mt-4 p-0">
                  <p class="c-text-2 soft-title medium-weight">Address</p>
                  <input id="address" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Address...">
              </div>
              <div class="col-12 mt-4 p-0">
                  <p class="c-text-2 soft-title medium-weight">Additional Information</p>
                  <textarea id="info" class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b"rows="4" style="width: 100%; min-height: 150px;"></textarea>
              </div>
              <button id="btnAdd" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2">Add Supplier</button>
          </div>
        </div>
</div>

<div class="modal fade" id="addMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Add Incoming Material</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <p class="c-text-2 soft-title regular-weight">Material Name</p>
            <input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name...">
        </div>
        <div class="col-12 p-0 mt-3">
            <p class="c-text-2 soft-title regular-weight">Material Quantity</p>
            <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative c-text-2 mr-3 medium-weight" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn-modal-positive c-text-2 medium-weight">Add Material</button>
      </div>
    </div>
  </div>
</div>
<script>
  $("#discount").select2();
  set_dropdown();
  setData();
  
  function setData() {
            var id = '<?php echo $id ?>';
            console.log(id);
             $.ajax({
                 type: "get",
                 async : true,
                 url: "http://153.92.4.88:8080/customer/"+id,
                 dataType: "text",
                 success: function (response) {
                     const context = JSON.parse(response);
                     const con = context.data[0];
                     var getInfo = con.customer_info;
                     var getAddress = con.customer_address;

                     if (getInfo == "null") {
                       getInfo = "";
                     }
                     if (getAddress == "null") {
                       getAddress = "";
                     }

                     $("#name").val(con.customer_name);
                     $("#info").val();
                     $("#id").val(con.customer_id);
                     $("#address").val(getAddress);
                     $("#discount option:selected").val(con.customer_discount_id);
                     $("#type option:selected").text(con.customer_type);
                 }
             });
        }

  function set_dropdown() {
        $.ajax({
            type: "GET",
            url: "http://153.92.4.88:8080/discount",
            async: true,
            dataType: "text",
            success: function (response) {
                var payload = '';
                var i;
                obj = JSON.parse(response);
                for(i=0; i<obj.data.length; i++){
                    payload += '<option value="'+obj.data[i].discount_id+'">'+obj.data[i].discount_percentage+'%</option>';
                    //$("#matName").append(payload);
                    $("#discount").html(payload);
                }
            }
        });
    }

  $("#btnAdd").click(function (e) { 
    e.preventDefault();
    
    var id  = '<?php echo $id ?>';
    var name  = $('#name').val();
    var type  = $('#type option:selected').text();
    console.log(type);
    var info  = $('#info').val();
    var address = $("#address").val();
    var getRaw = $("#discount option:selected").text();
    var getDis = getRaw.split("%");
    var getDisId = $("#discount option:selected").val();
    var id = $("#id").val();

    if (id.length < 1) {
        alert("Fill field customer id !");
    }
    if (info.length < 1) {
        info = "null";
    }
    if (address.length < 1) {
        address = "null";
    }  

    request = $.ajax({
                    url: 'http://153.92.4.88:8080/customer/'+id,
                    type: 'put',
                    data: {
                        customer_id: id,
                        customer_name: name,
                        customer_address: address,
                        customer_type: type,
                        customer_additional_info: info,
                        customer_discount_id: getDisId,
                        customer_discount_percentage: getDis[0]
                    }
                });

    request.done(function(response) {
        window.location.href = "<?php echo base_url('index.php/c_customer') ?>";
    });
    request.fail(function(response) {
        var success = response.success;
        var message = response.message;
        var data = response.data;
    });

  });
</script>
