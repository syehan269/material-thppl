    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <input class="search-fill col-6 border-0" type="text" name="search" id="search" placeholder="Search">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
        <a href="<?php echo base_url() ?>index.php/c_customer/input">
            <button class="c-text-2 my-auto btn-add c-color-primary text-white medium-weight">
                Add New Customer
            </button>
        </a>
        <div class="ml-auto d-inline-flex">
            <div class="d-inline-flex my-auto">
                <p class="my-auto c-text-2 mr-3 medium-weight">Customer Type</p>
                <!-- <input class="my-auto ml-3 c-text-2 search-fill col-5 border-0" type="text" name="searh" placeholder="All material"> -->
                <select name="name" id="type" style="width: 100%" class="p-2 dropdown-select2 c-text-2 search-fill" >
                    <option value="">Select Customer</option>
                    <option value="Distributor">Distributor</option>
                    <option value="Retail">Retail</option>
                    <option value="Special Retail">Special Retail</option>
                </select>
            </div>
            <div class="d-flex my-auto ml-3">
                <button class="btn-filter c-color-primary" id="filter">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                </button>
            </div>
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="cust-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">Customer Name</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Customer Type</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Address</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Additional Info</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Action</td>
                </tr>
            </thead>
            <tbody id="show-in-table">

            </tbody>
        </table>
    </div>

    <div class="modal fade" id="prevCust" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Preview Customer</p>
            </div>
            <div class="modal-body c-main-background">
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Customer Name</p>
                    <input disabled id="prvName" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Customer Type</p>
                    <input disabled id="prvType" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Customer Discount</p>
                    <input disabled id="prvDis" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Customer Address</p>
                    <input disabled id="prvAddre" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Additional Description</p>
                    <textarea disabled id="prvInfo" class="search-fill c-card c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" style="min-height: 150px; width: 100%"></textarea>
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-Customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Delete Customer </p>
            </div>
            <div class="modal-body c-main-background">
                <a>Apakah anda ingin menghapus data customer <span id="Customer-delete-name"></span></a>
                <input id="id-delete" type="hidden">
                <hr>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Cancel</button>
                <button id="btn-delete" class="btn-modal-positive medium-weight">Confirm</button>
            </div>
            </div>
        </div>
    </div>


</div>
<script>
    $(document).ready(function () {
        //js select2 dropdown
        var table = "";
        $('.dropdown-select2').select2();
        show_data();

        $("#search").on("input", function () {
            search(1, this.value);
        });

        $("#type").on("change", function () {
            search(2, $("#type option:selected").val())
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#cust-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#type").val("");
            $("#search").val("");
        });

        function show_data(){
            $.ajax({
                type  : 'GET',
                url   : 'http://153.92.4.88:8080/customer',
                async : true,
                dataType : 'text',
                success : function(data){
                    var html = '';
                    var i;
                    var text = data;
                    obj = JSON.parse(text);
                    for(i=0; i<obj.data.length; i++){
                        var address = obj.data[i].customer_address;
                        if (address == "null" || address == "empty") {
                            address = "-";
                        }
                        html += '<tr>'+
                                    '<td class="p-3 c-text-2">'+(i + 1)+'</td>'+
                                    '<td class="p-3 c-text-2">'+obj.data[i].customer_name+'</td>'+
                                    '<td class="p-3 c-text-2">'+obj.data[i].customer_type+'</td>'+
                                    '<td class="p-3 c-text-2">'+address+'</td>'+
                                    '<td class="p-3 c-text-2">-</td>'+
                                    '<td>'+
                                        '<a href="#" class="" onclick="show_preview(\''+obj.data[i].customer_name+'\',\''+obj.data[i].customer_address+'\',\''+obj.data[i].customer_type+'\',\''+obj.data[i].customer_additional_info+'\',\''+obj.data[i].customer_discount_percentage+'\')">'+
                                            '<button class="ml-2 basic-btn c-color-primary">'+
                                                '<i class="bx bx-show text-white" style="margin-top: 5px"></i>'+
                                            '</button>'+
                                        '</a>'+
                                        '<a href="<?php echo base_url('index.php/c_customer/edit'); ?>?id='+obj.data[i].customer_id+'" class="" >'+
                                            '<button class="ml-2 basic-btn c-soft-background">'+
                                                '<i class="bx bxs-pencil primary-title" style="margin-top: 5px"></i>'+
                                            '</button>'+
                                        '</a>'+
                                        '<a href="#" onclick="delete_Customer(\''+obj.data[i].customer_id+'\',\''+obj.data[i].customer_name+'\')" class="" >'+
                                            '<button class="ml-2 basic-btn-1 c-soft-background" style="border=solid 1px #5756B3 !important">'+
                                                '<i class="bx bxs-trash primary-title" style="margin-top: 5px"></i>'+
                                            '</button>'+
                                        '</a>'+
                                    '</td>'+
                                '</tr>';                        
                    }
                    $('#show-in-table').html(html);
                    dataTable();
                }
            });
        }

        function dataTable(){
            table = $('#cust-table').DataTable({
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

    });

    function show_preview(name, address, type, info, discount) {
        if (info == "null" || info == "empty") {
            info = "";
        }
        if (address == "null" || address == "empty") {
            address = "";
        }
        discount = discount+"%";
        $("#prvDis").val(discount);
        $("#prvName").val(name);
        $("#prvType").val(type);
        $("#prvAddre").val(address);
        $("#prvInfo").val(info);
        $('#prevCust').modal("show");
    }

    function delete_Customer(id, name){
        $('#id-delete').val(id);
        $('#Customer-delete-name').html(name);
        $('#delete-Customer').modal("show");
    }
</script>