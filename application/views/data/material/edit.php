   <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Edit Material</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <form action="" method="post" class="col-12 p-0">
                <div class="flex-column col-12 main-padding-l pr-0">
                    <div class="col-12 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Material ID</p>
                        <input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="ID..." id="id">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Material Name</p>
                        <input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name..." id="material">
                    </div>
                    <div class="col-12 p-0 mt-4">
                        <p class="c-text-2 soft-title medium-weight">Brand Name</p>
                        <select name="name" id="brand" style="width: 100%" class="dropdown-select2 c-text-2 search-fill" >
                            <option value="">Select Brand</option>
                        </select>
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Price</p>
                        <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Price..." id="price">
                    </div>
                    <button id="btnSubmit" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2" type="submit">Add Material</button>
                </div>
            </form>
        </div>
</div>
<script>
    $("#brand").select2();
    setBrandDrop();
    setData();

    function setData() {
            var id = '<?php echo $id ?>';
            console.log(id);
             $.ajax({
                 type: "get",
                 async : true,
                 url: "http://153.92.4.88:8080/material/"+id,
                 dataType: "text",
                 success: function (response) {
                    const context = JSON.parse(response);
                    $("#brand option:selected").val(context.data[0].material_merek_id);
                    $("#material").val(context.data[0].material_name);
                    $("#id").val(context.data[0].material_id);
                    $("#price").val(context.data[0].material_price);
                 }
             });
        }

   $("#btnSubmit").click(function (e) { 
        e.preventDefault();
        var id = '<?php echo $id ?>'
        var getMaterial = $("#material").val();
        var getID = $("#id").val();
        var getBrand = $("#brand option:selected").val();
        var getBrandName = $("#brand option:selected").text();
        var getPrice = parseInt($("#price").val());

        request = $.ajax({
                    url: 'http://153.92.4.88:8080/material/'+id,
                    type: 'put',
                    data: {
                        material_name: getMaterial,
                        material_merek_name: getBrandName,
                        material_price: getPrice,
                        material_merek_id: getBrand
                    }
        });

        request.done(function(response) {
            window.location.href = "<?php echo base_url() ?>index.php/c_material";
        });
        request.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;
            console.log(success);
            console.log(message);
            console.log(data);
        });

    });

    function setBrandDrop() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/merek",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    for(i=0; i<obj.data.length; i++){
                        payload += '<option value="'+obj.data[i].merek_id+'">'+obj.data[i].merek_name+'</option>';
                        //$("#matName").append(payload);
                        $("#brand").html(payload);
                    }
                }
            });
        }
</script>