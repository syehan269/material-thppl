    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <input class="search-fill col-6 border-0" type="text" name="search" id="search" placeholder="Search">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
        <a href="<?php echo base_url() ?>index.php/c_merk/input">
            <button class="c-text-2 my-auto btn-add c-color-primary text-white medium-weight">
                Add New Brand
            </button>
        </a>
        <div class="ml-auto d-inline-flex">
            <div class="d-inline-flex my-auto">
                <!-- <p class="my-auto c-text-2 mr-3 medium-weight">Supplier Name</p> -->
                <!-- <input class="my-auto ml-3 c-text-2 search-fill col-5 border-0" type="text" name="searh" placeholder="All material"> -->
                <!-- <select name="name" id="supplier" style="width: 100%" class="d-none dropdown-select2 c-text-2 search-fill" >
                    <option id='item' value="">Select Supplier</option>
                </select> -->
            </div>
            <!-- <div class="d-flex my-auto ml-3">
                <button class="btn-filter c-color-primary" id="filter">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                </button>
            </div> -->
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="material-table">
            <thead>
                <tr class="t-header border-0 col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight">Brand ID</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Brand</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Create Time</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Description</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Action</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Supplier</td>
                </tr>
            </thead>
            <tbody id="show-in-table">

            </tbody>
        </table>
    </div>

    <div class="modal fade" id="prevMerk" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Preview Brand</p>
            </div>
            <div class="modal-body c-main-background">
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Brand Id</p>
                    <input disabled id="prvId" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Brand Name</p>
                    <input disabled id="prvName" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>


</div>
<script>
    $(document).ready(function(){

        //js select2 dropdown
        $('.dropdown-select2').select2();
        var table = "";

        show_data();
        set_filter();

        $("#search").on("input", function () {
            var getIn = this.value;
            search(2, getIn);
        });

        $("#supplier").on("change", function () {
            var getIn = $("#supplier option:selected").text();
            search()
        });

        function show_data(){

            $.ajax({
                type  : 'GET',
                url   : 'http://153.92.4.88:8080/merek/',
                async : true,
                dataType : 'text',
                success : function(data){
                    var html = '';
                    var i;
                    var text = data;
                    obj = JSON.parse(text);
                    for(i=0; i<obj.data.length; i++){
                        var getDate = obj.data[i].merek_insert_date;
                        var split = getDate.split("T")
                        var split1 = split[0].split("-")
                        var setDate = split1[1]+"/"+split1[2]+"/"+split1[0];
                        
                        html += '<tr>'+
                                    '<td class="p-3 c-text-2">'+(i + 1)+'</td>'+
                                    '<td class="p-3 c-text-2">'+obj.data[i].merek_id+'</td>'+
                                    '<td class="p-3 c-text-2">'+obj.data[i].merek_name+'</td>'+
                                    '<td class="p-3 c-text-2">'+setDate+'</td>'+
                                    '<td class="p-3 c-text-2">-</td>'+
                                    '<td>'+
                                        '<a href="#" class="" onclick="show_preview(\''+obj.data[i].merek_id+'\',\''+obj.data[i].merek_name+'\',\''+obj.data[i].merek_insert_date+'\')">'+
                                            '<button class="ml-2 basic-btn c-color-primary">'+
                                                '<i class="bx bx-show text-white" style="margin-top: 5px"></i>'+
                                            '</button>'+
                                        '</a>'+
                                        '<a href="<?php echo base_url('index.php/c_merk/edit'); ?>?parameter1='+obj.data[i].merek_id+'" class="" >'+
                                            '<button class="ml-2 basic-btn c-soft-background">'+
                                                '<i class="bx bxs-pencil primary-title" style="margin-top: 5px"></i>'+
                                            '</button>'+
                                        '</a>'+
                                    '</td>'+
                                    '<td class="p-3 c-text-2">-</td>'+
                                '</tr>';                        
                    }
                    
                    $('#show-in-table').html(html);

                    dataTable();
                }
            });
        }

        function dataTable(){
            table = $('#material-table').DataTable({
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },
                "columnDefs": [
                    {
                        "targets": [6],
                        "visible": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function set_filter() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/suppliers",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].supplier_name+'>'+obj.data[i].supplier_name+'</option>';
                        $("#supplier").html(payload);
                    }
                }
            });
        }

    });

    function show_preview(id, name, time){
        $("#prvId").val(id);
        $("#prvName").val(name);
        $("#prvTime").val(time);
        $('#prevMerk').modal("show");
    }
    
</script>