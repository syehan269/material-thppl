    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Add Material Brand</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <form action="" method="post" class="col-12 p-0">
                <div class="flex-column col-12 main-padding-l pr-0">
                    <div class="col-12 p-0">
                        <p class="c-text-2 soft-title medium-weight">Brand</p>
                        <input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="brand" placeholder="Brand...">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Additional Info</p>
                        <textarea class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" id="info" style="width: 100%; min-height: 150px;"></textarea>
                    </div>
                    <button id="btnSubmit" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2" type="submit">Add Material</button>
                </div>
            </form>
        </div>
</div>

<script>
    $(document).ready(function () {
        $('.dropdown-select2').select2(); 
        $('#material').select2({
            maximumSelectionLength: 4
        }); 
        set_filter();
        set_supplier();

        function set_filter() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/material",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].material_name+'>'+obj.data[i].material_name+'</option>';
                        $("#material").html(payload);
                    }
                }
            });
        }

        $("#btnSubmit").click(function (e) { 
            e.preventDefault();
            var getMaterial = $("#material").select2("val");
            var getBrand = $("#brand").val();
            var getSupplier = $("#supplier").val();
            var getInfo = $("#info").val();

            request = $.ajax({
                        url: 'http://153.92.4.88:8080/merek',
                        type: 'post',
                        data: {
                            merek_name: getBrand
                        }
                    });

          request.done(function(response) {
              window.location.href = "<?php echo base_url() ?>index.php/c_merk";
          });
          request.fail(function(response) {
              var success = response.success;
              var message = response.message;
              var data = response.data;
          });

        });

    });
</script>