    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4" id="title">Edit Outcome Transaction</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <div class="col-6 main-padding-r">
                <button data-toggle="modal" data-target="#addMaterial" type="button" class="btn-outline col-12 c-border-primary primary-title c-main-background c-text-2 boldest-weight">
                    Add Material
                </button>
                <div class="mt-4 custom-card p-3" style="min-height: 400px !important;">
                    <table class="col-12 p-3" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Brand</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Quantity</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Price</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Total</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody id="tbMaterial">
                            
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="flex-column col-6 main-padding-l pr-0">
                <div class="col-12 p-0">
                    <p class="c-text-2 soft-title medium-weight">Buyer Name</p>
                    <!-- <input class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name..."> -->
                    <select name="name" id="name" style="width: 100%" class="dropdown-select2 c-text-2 search-fill" >
                        <option value="">Select Supplier</option>
                    </select>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Payment Method</p>
                    <!-- <input class="col-11 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Payment..."> -->
                    <select class="c-dropdown col-11 c-text-2" id="payment">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Cash">Cash</option>
                        <option class="dropdown-item" value="Credit">Credit</option>
                    </select>
                    <a href="#">
                        <button id="btnCicilan" data-toggle="modal" data-target="#addCicilan" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                            <i class="bx bxs-pencil bx-xs text-white" style="margin-top: 5px"></i>
                        </button>
                    </a >
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Service Type</p>
                    <select id="delivery" class="c-dropdown col-12 c-text-2" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Delivery">Delivery</option>
                        <option class="dropdown-item" value="Take Away">Take Away</option>
                    </select>
                </div>
                <div class="d-inline-flex col-12 p-0">
                    <div class="col-5 mt-4 p-0 mr-3">
                        <p class="c-text-2 soft-title medium-weight">Discount</p>
                        <input id="discountForm" class="col-12 c-text-2 secondary-field main-padding-l main-padding-r" value="1% - Rp 45.000" disabled>
                    </div>
                    <div class="col-6 ml-auto mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Total Payment</p>
                        <div class="p-0 d-inline-flex col-12">
                            <input id="amount" class="col-10 c-text-2 secondary-field main-padding-l main-padding-r" value="Rp 0" disabled>
                            <a href="#" class="my-auto">
                                <button id="btnRefresh" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                                    <i class="bx bx-refresh bx-xs text-white" style="margin-top: 5px"></i>
                                </button>
                            </a >
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Additional Description</p>
                    <textarea id="info" class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" style="min-height: 150px; width: 100%"></textarea>
                </div>
                <button id="btn" class="btn-add col-12 c-text-2 text-white c-color-primary c-color-primary mt-4">Edit Outcome Transaction</button>
            </div>
        </div>
</div>

<div class="modal fade" id="addCicilan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Edit Installment Payment </p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <p class="c-text-2 soft-title regular-weight">Payment Count</p>
            <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" value="0" id="paymentCount">
        </div>
        <hr>
        <div class="col-12 p-0" id="countPay">

        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Cancel</button>
        <button type="button" id="addNewCicilan" class="btn-modal-positive medium-weight">Confirm</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Add Outcoming Material</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <div class="d-inline-flex col-12 px-0">
                <input type="text" id="search-in" class="col-6 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Search Brand...">
                <div class="ml-3 px-0 col-5">
                    <select class="c-dropdown col-4 c-text-2 " id="filter-material" style="width: 60%">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="">Select Material</option>
                    </select>
                </div>
                <div class="d-flex my-auto">
                    <button class="btn-filter c-color-primary" id="filter">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                    </button>
                </div>
            </div>
            <div class="mt-4 custom-card p-3">
                    <table id="addNewTable" class="col-12 p-2" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center">No.</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Brand</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Quantity</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Price</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody id="materialBody">
                            
                        </tbody>
                    </table>
                </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Cancel</button>
        <!-- <button id="subMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modQuantMaterial" tabindex="-1" role="dialog" aria-labelledby="close" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Add Outcoming Material</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0 mt-2">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material Quantity</p>
            <input type="number" id="editQuant" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material brand</p>
            <input type="text" id="editBrand" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material Price</p>
            <input type="text" id="editPrice" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material name</p>
            <input type="text" id="editName" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button id="show_again" type="button" class="btn-modal-negative c-text-2 mr-3 medium-weight" data-dismiss="modal">Back</button>
        <button id="addMaterialMod" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function () {
        var table="";
        $('.dropdown-select2').select2();
        $('#filter-material').select2();

        $("#search-in").on("input", function () {
            search(2, this.value);
        });

        $("#filter-material").on("change", function () {
            search(1, this.value);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#addNewTable').DataTable()
                .search('').columns()
                .search('').draw();
            $("#filter-material-out").val("");
            $("#search-in").val("");
        });

        set_filter();
        set_dropdown_material();
        set_dropdown_brand();
        show_data();
        set_data();

        function dataTable(){
            table = $('#addNewTable').DataTable({
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function set_dropdown_brand(){
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/merek",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].merek_name+'>'+obj.data[i].merek_name+'</option>';
                        $("#brandName").html(payload);
                    }
                }
            });
        }

        function set_filter() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/customer",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].customer_name+'>'+obj.data[i].customer_name+'</option>';
                        $("#name").html(payload);
                    }
                }
            });
        }

        function set_dropdown_material() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/material",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].material_name+'>'+obj.data[i].material_name+'</option>';
                        $("#matName").html(payload);
                        $("#filter-material").html(payload);
                    }
                }
            });
        }

        function show_data(){

            $.ajax({
                type  : 'GET',
                url   : 'http://153.92.4.88:8080/material',
                async : true,
                dataType : 'text',
                success : function(data){
                    var html = '';
                    var i;
                    var text = data;
                    obj = JSON.parse(text);
                    for(i=0; i<obj.data.length; i++){
                        var setId = "new-mat-"+i;
                        var setMate = setId+"-mate";
                        var setBrand = setId+"-brand";
                        var setQuant = setId+"-quant";
                        var setPrice = setId+"-price";
                        html += '<tr>'+
                                    '<td class="p-3 c-text-2 text-center">'+(i + 1)+'</td>'+
                                    '<td id='+setMate+' class="p-3 c-text-2 text-center">'+obj.data[i].material_name+'</td>'+
                                    '<td id='+setBrand+' class="p-3 c-text-2 text-center">'+obj.data[i].material_merek_name+'</td>'+
                                    '<td id='+setQuant+' class="p-3 c-text-2 text-center">1</td>'+
                                    '<td id='+setPrice+' class="p-3 c-text-2 text-center">'+obj.data[i].material_price+'</td>'+
                                    '<td class="text-center">'+
                                        '<a href="#" class="" onclick="showQuant(\''+setMate+'\',\''+setBrand+'\',\''+setQuant+'\')">'+
                                            '<button class="ml-2 basic-btn c-color-primary">'+
                                                '<i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>'+
                                            '</button>'+
                                        '</a>'+
                                    '</td>'+
                                '</tr>';                        
                    }
                    $('#materialBody').html(html);
                    dataTable();
                }
            });
        }

        function set_data() {
            var tot = "<?php echo $id ?>";
            console.log(tot);
            $.ajax({
                type: "get",
                url: "http://153.92.4.88:8080/transaction-out/"+tot,
                async: true,
                dataType: "text",
                success: function (response) {  
                    var obj = JSON.parse(response);
                    var getId = obj.data.transaction.trans_out_id;
                    var getAmount = obj.data.transaction.trans_out_payment_amount;
                    var getType = obj.data.transaction.trans_out_payment_type;
                    var getInfo = obj.data.transaction.trans_out_additional_info;
                    var getDeli = obj.data.transaction.trans_out_delivery_type;

                    var rawDate = obj.data.transaction.trans_out_insert_date;
                    var split = rawDate.split("T")
                    var split1 = split[0].split("-")
                    var getDate = split1[1]+"/"+split1[2]+"/"+split1[0];
                    var payload = "";
                    var amount = "Rp "+getAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                    var getCountCredit = 0;

                    if (getInfo == "null" || getInfo == "COK") {
                        getInfo = "";
                    }

                    var getRow = $("#tb_material tr").length;
                    console.log(getRow);
                    var createID = "add"+"-"+(getRow+1);

                    $("#info").val(getInfo);
                    $("#amount").val(amount);

                    $("#payment option").filter(function() {
                        return $(this).text() == getType;
                    }).prop('selected', true);

                    $("#sellInteract option").filter(function() {
                        return $(this).text() == getDeli;
                    }).prop('selected', true);

                    $("#name option").filter(function() {
                        //may want to use $.trim in here
                        return $(this).text() == "";
                    }).prop('selected', true);

                    if (getType == "Credit") {
                        getCountCredit = obj.data.credit.length;
                        console.log(getCountCredit);
                        $("#paymentCount").val(getCountCredit);
                        addCicilanField();

                        for (let o = 0; o < obj.data.credit.length; o++) {
                            const kredit = obj.data.credit[o];
                            var getCreditDue = kredit.credit_due_date;
                            var split = getCreditDue.split("T")
                            var fieldId = "#cicilan"+(o+1);
                            console.log(fieldId);
                            $(fieldId).val(split[0]);
                        }

                    }

                    for (let i = 0; i < obj.data.detail.length; i++) {
                        const context = obj.data.detail[i];
                        var getQuant = context.detail_out_material_amount;
                        var getBrand = context.detail_out_merek_name;
                        var getMate = context.detail_out_material_name;
                        var getPrice = context.detail_out_material_price;
                        var getTotal = parseInt(getQuant)*parseInt(getPrice);
                        var createID = "add"+"-"+(i+1);
                        var payload = '<tr id="'+createID+'" class="t-item">'+
                                            '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getMate+'</td>'+
                                            '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-brand">'+getBrand+'</td>'+
                                            '<td class="p-2 c-text-2 text-center"><input id="'+createID+'-quant" class="text-center search-fill col-4" placeholder="Quantity..." value="'+getQuant+'"></td>'+
                                            '<td class="p-2 c-text-2 text-center" id="'+createID+'-price">'+getPrice+'</td>'+
                                            '<td class="p-2 c-text-2 text-center" id="'+createID+'-total">'+getTotal+'</td>'+
                                            '<td class="p-2 c-text-2 text-center ">'+
                                                '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                                    '<button class="ml-2 basic-btn c-soft-background" >'+
                                                        '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                                    '</button>'+
                                                '</a >'+
                                            '</td>'+
                                        '</tr>';
                        $("#tbMaterial").append(payload);
                    }

                }
            });
        }

        $("#payment").on("change", function () {
            var payment = $(this).find('option:selected').text()
            if (payment === "Cash") {
                $("#btnCicilan").prop("disabled", true);
            }else{
                $("#btnCicilan").prop("disabled", false);
            }
        });

        $("#btnCicilan").click(function (e) { 
            e.preventDefault();
            var payment = $("#payment").find('option:selected').text()
            if (payment === "Cash") {
                $("#btnCicilan").prop("disabled", true);
            }else{
                $("#btnCicilan").prop("disabled", false);
            }
        });

        $("#addNewCicilan").click(function (e) { 
            e.preventDefault();
            console.log(cicilanBundle());
        });

        $("#addMaterialMod").click(function (e) { 
            e.preventDefault();
            addNewMate()
        });

        $("#btnRefresh").click(function (e) { 
            e.preventDefault();
            totalQuantity();
        });

        $("#paymentCount").on("input", function () {
            addCicilanField();
        });

        $("#delivery").on("change", function () {
            var action = $(this).find('option:selected').val()
            if (action === "Delivery") {
                var form = '<div class="col-12 mt-4 p-0"><p class="c-text-2 soft-title regular-weight">Delivery Price</p>'+
                           '<input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deliveryPrice"></div>'+
                           '<div class="col-12 mt-4 p-0"><p class="c-text-2 soft-title regular-weight">Delivery Address</p>'+
                           '<input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deliveryAddress"></div>';

                $("#increasePrice").html(form);
                //set_main_data()
            }else{
                $("#increasePrice").html("");
            }
        });

        $("#subMaterial").click(function (e) { 
            e.preventDefault();
            var getName = $("#brandName option:selected").text();
            var getQuant = $("#matQuant").val();
            var getRow = $("table tbody tr").length;
            var getID = $("#brandName").val();
            var createID = getID+"-"+(getRow+1);

            var payload = '<tr id="'+createID+'" class="t-item">'+
                                '<td class="p-3 c-text-2 text-center">'+getName+'</td>'+
                                '<td class="p-3 c-text-2 text-center">'+getQuant+'</td>'+
                                '<td class="p-3 c-text-2 text-center ">'+
                                    '<a href="#">'+
                                        '<button class="ml-2 basic-btn c-color-primary" >'+
                                            '<i class="bx bx-show bx-xs text-white" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                    '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
                            
            $("#tbMaterial").append(payload);
            totalQuant();
        });

    });

    function getTotalPay() {
        var total = 0;
        var getDeli = $("#sellInteract option:selected").text();
        var getOngkir = 0;
        var getDis = $("#discountForm").val();
        var splitDis = getDis.split("%");

        $("#tbMaterial").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#temp-quant-"+(index+1);
            var totalId = "#"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();
            
            console.log(getQuant+" "+getPrice);
            var semi = parseInt(getQuant) * parseInt(getPrice);
            total += semi;
        });

        if (getDeli == "Delivery") {
            getOngkir = $("#deliveryPrice").val();
            console.log(getOngkir);
        }

        var persen = parseInt(splitDis[0])/100 * total;
        var final = total-persen;
        var final1 = final + parseInt(getOngkir);
        
        console.log(final1);
        return final1;

    }

    function addCicilanField() {
        var getCount = $("#paymentCount").val();
        var payload = '';
        console.log(getCount);
        for (let i = 0; i < getCount; i++) {
            const element = getCount;

            payload += '<p id="cicilan-number-'+(i+1)+'" class="field-cicilan c-text-2 soft-title mt-3 regular-weight">Payment '+(i + 1)+'</p>' +
                    '<input type="date" id="cicilan'+(i + 1)+'" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">';
            $("#countPay").html(payload);
        }
    }

    function materialBundle() {
        var material = [];

        $("#tbMaterial").find("tr").each(function (index, element) {
            // element == this
            var getId = $(element).find('td');
            var getId1 = $(element).find('td input');
            var quantId = "#add-"+(index+1)+"-quant";
            var getMatName = "null";
            var getQuant = $(quantId).val();
            var getMatId = "null";
            var getPrice = "10000";
            var getBraId = "null";
            var getBraName = getId.eq(0).text();

            var item = {};
            item.detail_out_material_name= getMatName;
            item.detail_out_material_id = getMatId;
            item.detail_out_material_amount = getQuant;
            item.detail_out_material_price = getPrice;
            item.detail_out_merek_id = getBraId;
            item.detail_out_merek_name = getBraName;
            material.push(item);            
            
        });
        
        return material;

    }

    function cicilanBundle() {
        var getCicilanBundle = [];
        $(".field-cicilan").each(function (index, element) {
            // element == this
            var id = "#cicilan-number-"+(index+1);
            console.log(id);
            var getCicilan = $(element).val();
            var getInfo = $(id).text();
            var item = {};
            var split1 = getCicilan.split("-");
            var getDate = getCicilan+" 00:00:00";

            item.credit_amount = 10000;
            item.credit_additional_info = getInfo;
            item.credit_due_date = getDate;
            getCicilanBundle.push(item);
        });
        return getCicilanBundle;
    }

    function addNewMate() {
        var getMate = $("#editName").val();
        var getBrand = $("#editBrand").val();
        var getQuant = $("#editQuant").val();
        var getPrice = $("#editPrice").val();
        var finPrice = getPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

        var getRow = $("#tbMaterial tr").length;
        console.log(getRow);

        var createID = "add"+"-"+(getRow+1);
        if (getQuant < 1) {
            getQuant = 1;
        }

        console.log(getMate+","+getQuant+","+getBrand+","+getRow);

         var payload = '<tr id="'+createID+'" class="t-item">'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getMate+'</td>'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getBrand+'</td>'+
                                '<td class="p-2 c-text-2 text-center"><input id="'+createID+'-quant" class="text-center search-fill col-4" placeholder="Quantity..." value="'+getQuant+'"></td>'+
                                '<td class="p-2 c-text-2 text-center ">'+
                                    '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
        $("#tbMaterial").append(payload);
        totalQuant();
    }

    function totalQuantity() {
        var total = 0;
        var getDeli = $("#sellInteract option:selected").text();
        var getOngkir = 0;
        var getDis = $("#discountForm").val();
        var splitDis = getDis.split("%");

        $("#tbMaterial").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#add-"+(index+1)+"-quant";
            var totalId = "#add-"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();
            
            console.log(getQuant+" "+getPrice);
            var semi = parseInt(getQuant) * parseInt(getPrice);
            total += semi;
        });

        if (getDeli == "Delivery") {
            getOngkir = $("#deliveryPrice").val();
            console.log(getOngkir);
        }

        var persen = parseInt(splitDis[0])/100 * total;
        var final = total-persen;
        var final1 = final + parseInt(getOngkir);
        var gim =  "Rp "+final1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        var gim1 =  splitDis[0]+"% - Rp "+persen.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    
        console.log(final1);
        $("#amount").val(gim);
        $("#discountForm").val(gim1);
    }

    function totalQuant() {
        var total = 0;

        $("#tbMaterial").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#add-"+(index+1)+"-quant";
            var getQuant = $(quantId).val();
            var semi = parseInt(getQuant) * 10000;
            total += semi;
        });

        var gim =  "Rp "+total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        $("#amount").val(gim);
    }

    function showQuant(matId, brandId, price) {
        var getMat = $("#"+matId).text();
        var getBra = $("#"+brandId).text();
        //var getPri = $("#"+priceId).text();
        
        $("#editName").val(getMat);
        $("#editPrice").val(price);
        $("#editBrand").val(getBra);

        $("#modQuantMaterial").modal("show");
        $("#addMaterial").modal("hide");
    }

    function showModMate() {
        $("#modQuantMaterial").modal("hide");
        $("#addMaterial").modal("show");
    }

    function deleteItem(id) {
        console.log(id);
        var fag = "#"+id;
        $(fag).remove();
        totalQuant();
    }

</script>
