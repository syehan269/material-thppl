    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-5">
            <input class="search-fill col-6 border-0" type="text" name="search" id="search" placeholder="Search">
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>
        
    <div class="d-inline-flex col-12 p-0">
        <?php if($this->session->userdata("user_level") == "kasir"): ?>
            <a href="<?php echo base_url() ?>index.php/c_income/input">
                <button class="c-text-2 my-auto btn-add c-color-primary text-white medium-weight">
                    Add New Transaction
                </button>
            </a>
        <?php else:?>
        <?php endif;?>
        <div class="ml-auto d-inline-flex">
            <div class="d-inline-flex my-auto">
                <p class="my-auto c-text-2 mr-3 medium-weight">Date</p>
                <input id="date" type="date" class="my-auto c-text-2 search-fill" placeholder="Date" style="padding: 10px 12px">
            </div>
            <div class="d-inline-flex my-auto ml-3 c-text-2">
                <p class="my-auto mr-3 c-text-2 medium-weight">Type</p>
                <div class="dropdown c-text-2">
                    <select class="c-dropdown" id="type-drop">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="">All</option>
                        <option class="dropdown-item" value="Cash">Cash</option>
                        <option class="dropdown-item" value="Credit">Credit</option>
                    </select>
                </div>
            </div>
            <div class="d-flex my-auto ml-3">
                <button class="btn-filter c-color-primary" id="filter">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                </button>
            </div>
        </div>
    </div>

    <div class="mt-4 custom-card p-3">
        <table width="100%" id="income-table">
            <thead>
                <tr class="t-header col-12">
                    <td class="p-3 primary-title c-text-2 boldest-weight">No</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Transaction ID</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Supplier</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Payment Type</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Payment Amount</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight d-none" >Quantity</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Date</td>
                    <td class="p-3 primary-title c-text-2 boldest-weight" >Action</td>
                </tr>
            </thead>
            <tbody id="tb_income">
                
            </tbody>
        </table>
    </div>

</div>
<div class="modal fade" id="prevIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Preview Income</p>
            </div>
            <div class="modal-body c-main-background px-4">
                <div class="col-12 p-0 ">
                        <p class="c-text-3 soft-title regular-weight">Supplier Name</p>
                        <input disabled id="prvName" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                    </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Transaction Type</p>
                    <input disabled id="prvTrans" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Transaction Date</p>
                    <input disabled id="prvDate" type="text" class="col-12 c-text-2 c-card search-fill main-padding-l main-padding-r">
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Material Purchased</p>
                    <div class=" col-12 custom-card p-3">
                        <table class="col-12 p-3" width="100%">
                            <thead class="t-header primary-title">
                                <tr>
                                    <th class="p-3 c-text-3 boldest-weight text-center">No.</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Material</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Brand</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Quantity</th>
                                    <th class="p-3 c-text-3 boldest-weight text-center">Price(1)</th>
                                </tr>
                            </thead>
                            <tbody id="tbPrv">
                                
                            </tbody>
                            <tfoot class="t-header primary-title" id="footMat">
                                
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-12 p-0 mt-3 ">
                    <p class="c-text-3 soft-title regular-weight">Transaction Information</p>
                    <textarea disabled id="prvInfo" class="search-fill c-card c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" style="min-height: 150px; width: 100%"></textarea>
                </div>
            </div>
            <div class="modal-footer c-main-background border-0">
                <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var table = "";
        //js select2 dropdown
        $('.dropdown-select2').select2();
        $("#search").on("input", function () {
            search(2, this.value);
        });

        $("#type-drop").on("change", function () {
            var type = $(this).find('option:selected').val();
            search(3, type);
        });

        $("#date").on("input change", function () {
            var getDate = this.value;
            var split1 = getDate.split("-");
            var finDate = split1[1]+"/"+split1[2]+"/"+split1[0];
            search(6, finDate);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#income-table').DataTable()
                .search('').columns()
                .search('').draw();
            $("#date").val("");
            $("#type-drop").val("");
            $("#search").val("");
        });

        setTable();

        function setTable() {
            $.ajax({
                type  : 'GET',
                url   : 'http://153.92.4.88:8080/transaction-in',
                async : true,
                dataType : 'text',
                success : function(data){
                    var html = '';
                    var i;
                    var text = data;
                    obj = JSON.parse(text);

                    for(i=0; i<obj.data.length; i++){
                        
                        var getDate = obj.data[i].trans_in_due_date;
                        var split = getDate.split("T");
                        var split1 = split[0].split("-");

                        var setDate = split1[1]+"/"+split1[2]+"/"+split1[0];
                        var result = obj.data[i].trans_in_payment_amount;
                        var amount = "Rp "+result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        var type = obj.data[i].trans_in_payment_type;

                        var rawId = obj.data[i].trans_in_id;
                        var transId = rawId.split("-");

                        var cashPay = "";

                        if (type == "Cash") {
                            cashPay = '<td class="p-3 c-text-2"><span class="cash-status">'+type+'</span></td>';
                        }else{
                            cashPay = '<td class="p-3 c-text-2"><span class="credit-status">'+type+'</span></td>';
                        }

                        html+='<tr>'+
                                '<td class="p-3 c-text-2">'+(i+1)+'</td>'+
                                '<td class="p-3 c-text-2">'+obj.data[i].trans_in_id+'</td>'+
                                '<td class="p-3 c-text-2">-</td>'+
                                cashPay+
                                '<td class="p-3 c-text-2">'+amount+'</td>'+
                                '<td class="p-3 c-text-2 d-none" id="quant-'+transId[1]+'"></td>'+
                                '<td class="p-3 c-text-2">'+setDate+'</td>'+
                                '<td>'+
                                    '<a href="#" onclick="setPreview(\''+obj.data[i].trans_in_id+'\')">'+
                                        '<button class="ml-2 basic-btn c-color-primary" >'+
                                            '<i class="bx bx-show text-white" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                    '<a href="<?php echo base_url('index.php/c_income/edit'); ?>?getId='+obj.data[i].trans_in_id+'" >'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-pencil primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
                        
                        setQuant(rawId);
                    }
                    $('#tb_income').html(html);
                    dataTable();
                }
            });
        }

        function dataTable(){
            table = $('#income-table').DataTable({
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function set_filter() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/material",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].material_name+'>'+obj.data[i].material_name+'</option>';
                        $("#material").html(payload);
                    }
                }
            });
        }

    });

    function setQuant(transId) {
        $.ajax({
            type: "get",
            url: "http://153.92.4.88:8080/transaction-in/"+transId,
            async: true,
            dataType: "text",
            success: function (response) {
                const obj = JSON.parse(response);
                var getTotal = 0;
                var id = transId.split("-");
                var loadId = "#quant-"+id[1];
                
                for (let i = 0; i < obj.data.detail.length; i++) {
                    const context = obj.data.detail[i];
                    var getAmount = context.detail_in_material_amount;
                    getTotal += getAmount;
                }

                var payload = '<span>'+getTotal+' Item</span>';
                //console.log(transId+" "+getTotal+" "+id);

                $(loadId).html(payload);

            }
        });
    }

    function setPreview(id) {
        $.ajax({
            type: "get",
            url: "http://153.92.4.88:8080/transaction-in/"+id,
            async: true,
            dataType: "text",
            success: function (response) {  
                var obj = JSON.parse(response);
                var getId = obj.data.transaction.trans_in_id;
                var getAmount = obj.data.transaction.trans_in_payment_amount;
                var getType = obj.data.transaction.trans_in_payment_type;
                var getInfo = obj.data.transaction.trans_in_additional_info;

                var rawDate = obj.data.transaction.trans_in_due_date;
                var split = rawDate.split("T")
                var split1 = split[0].split("-")
                var getDate = split1[1]+"/"+split1[2]+"/"+split1[0];
                var payload = "";
                var payloadMatFoot = "";
                var getItem = 0;

                var amount = "Rp "+getAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                if (getInfo == "null" || getInfo == "empty" || getInfo == null) {
                    getInfo = "";
                }

                $("#prvInfo").val(getInfo);
                $("#prvDate").val(getDate);
                $("#prvTrans").val(getType);

                for (let i = 0; i < obj.data.detail.length; i++) {
                    const context = obj.data.detail[i];
                    var getfagg = context.detail_in_material_amount;
                    var getPrice = context.detail_in_material_price;
                    var single = "Rp "+getPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    payload += '<tr>'+
                                    '<td class="p-2 c-text-2 text-center">'+(i+1)+'</td>'+
                                    '<td class="p-2 c-text-2 text-center">'+context.detail_in_material_name+'</td>'+
                                    '<td class="p-2 c-text-2 text-center">'+context.detail_in_merek_name+'</td>'+
                                    '<td class="p-2 c-text-2 text-center">'+context.detail_in_material_amount+' item </td>'+
                                    '<td class="p-2 c-text-2 text-center">'+single+'</td>'+
                            '</tr>';
                    getItem += getfagg;
                    $("#prvName").val(obj.data.detail[0].detail_in_supplier_name);
                }

                payloadMatFoot = '<tr>'+
                                    '<td colspan="3" class="p-3 c-text-3 boldest-weight text-center">Total</td>'+
                                    '<td class="p-3 c-text-3 boldest-weight text-center"> '+getItem+' item</td>'+
                                    '<td class="p-3 c-text-4 boldest-weight text-center"> '+amount+'</td>'+
                                '</tr>';

                        
                $("#footMat").html(payloadMatFoot);
                $("#tbPrv").html(payload);

            }
        });
        show_preview()
    }

    function show_preview() {
        $("#prevIn").modal("show");
    }
</script>
