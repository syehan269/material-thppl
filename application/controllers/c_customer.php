<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_customer extends CI_Controller
    {
        public function index(){
            $data['site'] = "customer";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('data/customer/customer');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $data['site'] = "customer";
            $send['id'] = $this->input->get('id');
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('data/customer/input');
            $this->load->view('header-footer/footer');
        }

        public function edit(){
            $send['id'] = $this->input->get('id');
            $send['site'] = "customer";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/customer/edit');
            $this->load->view('header-footer/footer');
        }

    }

?>