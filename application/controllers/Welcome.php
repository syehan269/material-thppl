<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$data['site'] = "dashboard";
		$this->load->view('header-footer/header');
		$this->load->view('sidebar-topbar/side', $data);
		$this->load->view('dashboard/dashboard');
		$this->load->view('header-footer/footer');
	}
	public function login()
	{	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true) {
			$data['username'] = $this->input->post('username', true);
			$data['password'] = $this->input->post('password', true);

			if ($data['username'] == 'admin' && $data['password'] == 'admin') {
				$session_data = array(
					'name' => $data['username'],
					'user_level' => 'admin'
				);
				$this->session->set_userdata($session_data);
				redirect('index.php/Welcome');
			}else if ($data['username'] == 'kasir' && $data['password'] == 'kasir') {
				$session_data = array(
					'name' => $data['username'],
					'user_level' => 'kasir'
				);
				$this->session->set_userdata($session_data);
				redirect('index.php/Welcome');
			}else {
				
			}

		}else {
			
		}

		$this->load->view('header-footer/header');
		$this->load->view('login');
	}

	public function logout()
	{
		$session_data = array(
			'user_level' => null
		);
		redirect(base_url("index.php/Welcome/login"));  
	}
}
