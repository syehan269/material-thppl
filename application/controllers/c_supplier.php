<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_supplier extends CI_Controller
    {
        public function index(){
            $send['site'] = "supplier";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/supplier/supplier');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "supplier";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/supplier/input');
            $this->load->view('header-footer/footer');
        }

        public function edit(){
            $send['data_id'] = $this->input->get('parameter1');
            $send['data_name'] = $this->input->get('parameter2');
            $send['data_telp'] = $this->input->get('parameter3');
            $send['data_fax'] = $this->input->get('parameter4');
            $send['data_address'] = $this->input->get('parameter5');
            $send['site'] = "supplier";

            $this->load->view('header-footer/header', $send);
            $this->load->view('sidebar-topbar/side');
            $this->load->view('data/supplier/edit',$send);
            $this->load->view('header-footer/footer');
        }
    }

?>