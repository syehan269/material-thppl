<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_material extends CI_Controller
    {
        public function index(){
            $send['site'] = "material";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/material/material');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "material";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/material/input');
            $this->load->view('header-footer/footer');
        }

        public function edit(){
            $send['id'] = $this->input->get('id');
            $send['site'] = "material";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('data/material/edit');
            $this->load->view('header-footer/footer');
        }

    }
    

?>