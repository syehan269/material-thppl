<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_outcome extends CI_Controller
    {
        public function index(){
            $send['site'] = "transaction_out";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('transaction/outcome/outcome');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "transaction_out";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('transaction/outcome/input');
            $this->load->view('header-footer/footer');
        }

        public function edit(){
            $send['site'] = "transaction_out";
            $send['id'] = $this->input->get('getId');
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('transaction/outcome/edit');
            $this->load->view('header-footer/footer');
        }

    }
    

?>